import React from 'react';

// react-bootstrap components
import { Col, Container, Row } from 'react-bootstrap';

// Components
import CourseCard from '../components/CourseCard';

// Data
import coursesData from '../data/courseData';

const Courses = () => {
    return (
        <Container fluid className="mx-auto">
            <h1 className="my-5">Courses</h1>
            <Row className="m-3">
            {
                coursesData.map(course => {
                    return (
                        <Col key={course.id} xs={12} sm={4}>
                            <CourseCard 
                                title={course.name}
                                desc={course.description}
                                price={course.price}
                            />
                        </Col>
                    )
                })
            }
            </Row>
        </Container>
    )
}

export default Courses;
