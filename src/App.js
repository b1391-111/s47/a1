import React from 'react';
import { Container } from 'react-bootstrap';
// CSS
import './App.css';
// Components
import Navbar from './components/Navbar';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';


function App() {
  return (
    <div className="App">
      <Navbar />
      <Container>
        <Home />  
        <Courses />
        <Register />
        <Login />
      </Container>
    </div>
  );
}

export default App;