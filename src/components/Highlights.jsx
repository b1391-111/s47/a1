import React from 'react';
import {
    Container, 
    Row,
} from 'react-bootstrap';
import Cards from './Card';

const Highlights = () => {
    const lorem = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi est ipsa eos iusto, architecto numquam quidem quos, iste, necessitatibus ea assumenda quia porro ut ducimus dolorem animi quaerat dolorum reprehenderit!";
    const data = [
        {
            id: 1,
            title: "Learn from Home",
            desc: lorem,
            image: "https://www.oberlo.com/media/1603970279-pexels-photo-3.jpg?fit=max&fm=jpg&w=1824"
        },
        {
            id: 2,
            title: "Study Now, Pay Later",
            desc: lorem,
            image: "https://archive.org/download/06-07-2016_Images_Images_page_1/02_PT_hero_42_153645159.jpg"
        },
        {
            id: 3,
            title: "Be Part of Our Community",
            desc: lorem,
            image: "https://images.unsplash.com/photo-1612151855475-877969f4a6cc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aGQlMjBpbWFnZXxlbnwwfHwwfHw%3D&w=1000&q=80"
        },
    ];

    return (
        <Container fluid className="mx-auto">
            <h1 className="my-5">Highlights</h1>
            <Row className="m-3">
                {
                    data.map(({id, title, desc, image}) => {
                        return (
                            <Cards
                                key={id}
                                title={title} 
                                desc={desc} 
                                image={image}
                            />
                        )
                    })
                }
            </Row>
        </Container>
    )
}

export default Highlights;
