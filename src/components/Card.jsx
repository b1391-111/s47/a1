import React from 'react'
import {
    Col,
    Card
} from 'react-bootstrap';
import './card.styles.css';

const Cards = ({id, title, desc, image}) => {
    return (
        <Col key={id} xs={12} sm={4}>
            <Card className="cardHighlights">
                <Card.Img src={image} />
                <Card.Body>
                    <Card.Title>{title}</Card.Title>
                    <Card.Text>
                        {desc}
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    )
}

export default Cards;
